FROM debian:8
MAINTAINER <tom@tomdavidson.org>

RUN apt-get update && apt-get upgrade -y    \
    && apt-get -y install --no-install-recommends \
        lcov                                \
        git                                 \
        curl                                \
        wget                                \
        unzip                               \
        python-pip                          \
    && pip install --upgrade pip            \
    && apt-get clean                        \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade awscli

RUN export TERRAFORM_VERSION=0.9.0 \
    && wget -P /tmp https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin \
    && rm -rf /tmp/* &&  rm -rf /var/tmp/*

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
    && apt-get -y install --no-install-recommends nodejs

COPY package.json .

RUN npm install --global yarn && yarn
